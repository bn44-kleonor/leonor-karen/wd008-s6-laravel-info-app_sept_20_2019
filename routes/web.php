<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'PageController@index');

Route::get('/About', 'PageController@about');

// Route::get('/Services', 'PageController@services')->middleware('isAdmin');
Route::get('/Services', 'PageController@services')->middleware('isAdmin');

Route::get('/home', 'HomeController@index')->name('home');


// Route::get('/posts', 'PostController@index')->middleware('auth');;
// Route::get('/posts/{id}', 'PostController@show')->middleware('auth');
// Route::get('/comments/{id}', 'CommentController@show');
// Route::delete('/posts/{id}', 'PostController@destroy');


Route::group(['middleware' => 'auth'], function(){
	//all registered user routes
	Route::get('/posts', 'PostController@index');
	// Route::post('/comments', 'CommentController@show');
	Route::post('/comments', 'CommentController@store');
	Route::get('/posts/{id}', 'PostController@show');
	Route::get('/comments/{id}', 'CommentController@show');
});


Route::group(['middleware' => ['auth','isAdmin']], function(){
	Route::post('/posts', 'PostController@store');
	Route::get('/posts/create','PostController@create');
	//PUT
	Route::put('posts/{id}','PostController@update');
    //EDIT
	Route::get('/posts/{id}/edit', 'PostController@edit');
	Route::delete('/posts/{id}', 'PostController@destroy');
	Route::delete('/comments/{id}','CommentController@destroy');
});


Route::delete('/posts/{id}', 'PostController@destroy');

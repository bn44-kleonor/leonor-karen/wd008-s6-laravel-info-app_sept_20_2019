<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use App\User;
use Session;
use Auth;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //Creating a method called index, and gagawin nya to display all post
        // $posts = Post::all();//you are making it eloquent(like select all on yopur database)
        // $posts = Post::where('title','Post 3')->get();
        // $posts = Post::orderBy('title', 'desc')->take(1)->get();
        // $posts = Post::orderBy('title', 'desc')->paginate(3);

        // dd(Auth::user()->id);   // 1
        // $posts = Post::where('user_id', Auth::user()->id)->orderBy('title', 'desc')->paginate(3);
        
        // $user = User::find(Auth::user()->id);
        // $posts = $user->posts;
        dd($posts);
        $comments = Comment::all();
        return view('posts.index', compact('posts','comments'));//compact as php function use to convert given keys convert to variable
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'body' => 'required'
        ]);
        //create new post
        $post = new Post;//instantiate
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = $request->input('user_id');
        $post->save();
        $id = $post->id;

        return redirect("/posts/$post->id")->with('success','Post was successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post =Post::find($id);
        return view('posts.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        return view('posts.edit')->with('post',$post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

  public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required'
        ]);

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = $request->input('user_id');
        $post->save();

        return redirect("/posts/$id")->with('success','Post was updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::find($id);
        $post_title = $post->title;
        $post->delete();
        Session::flash("success","$post_title has been deleted!");
        // return view('posts.index');

        //  $posts = Post::orderBy('created_at', 'desc')->paginate(3);
        // $comments = Comment::all();
        // return view('posts.index', compact('posts','comments'));
        return redirect()->action('PostController@index');
    }
}
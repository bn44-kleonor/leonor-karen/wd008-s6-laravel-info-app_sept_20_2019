<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //return index page
	public function index(){
		$title = "Wecome to my website";
		return view('pages.index')->with('x', $title);
	}

    //return about page
	public function about(){
		$title = "About page ito.";
		return view('pages.about')->with('y',$title) ;
	}
    
    //return services page
	public function services(){
		$data = array(
			'title' => 'Services Page',
			'services' => ['Web Design', 'Web Development', 'SEO']
		);
		return view('pages.services')->with($data);
	}
}

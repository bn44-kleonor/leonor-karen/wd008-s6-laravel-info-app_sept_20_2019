<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Post;
use App\Comment;
 
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('comments.addcomment');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // dd('tests');
        $this->validate($request,[
            'body' => 'required',
            'post_id'=>'required',
            'user_id'=>'required'
        ]);
        // dd('test');
        $comment = new Comment;
         $comment->post_id = $request->input('post_id');
        $comment->body = $request->input('body');
        $comment->user_id =$request->input('user_id');
        $comment->save();
        // dd('na save');
 
        return redirect()->back()->with('success','Post was successfully created');
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
            $comment = Comment::find($id);
            return view('comments.show')->with('comment', $comment);
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
            'body'=>'required',
            'post_id'=>'required',
            'user_id' => 'required'
        ]);
 
        $comment = new Comment;
        $comment->body = $request->input('body');
        $comment->post_id = $request->input('post_id');
        $comment->user_id =$request->input('user_id');
        $comment->save();
 
        return redirect("/comments/$id")->with('success','Comment was updated!');  //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $comment = Comment::find($id);
        $comment_id = $comment->$post_id;
        $comment->delete();
        Session::flash("success","$comment_id has been deleted");
        return redirect()->action('PostController@index');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //a post belongsTo one user
    public function user()
    {
    	return $this->belongsTo('App\User');

    }

    //a post has many comments
    public function comments()
    {
    	return $this->hasMany('App\Comment');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Post::insert([
        	['title' => 'Post 1', 'body' => 'This is the post 1 body', 'user_id' => 1],
        	['title' => 'Post 2', 'body' => 'This is the post 2 body', 'user_id' => 2],
        	['title' => 'Post 3', 'body' => 'This is the post 3 body', 'user_id' => 1],
            ['title' => 'Post 4', 'body' => 'This is the post 4 body', 'user_id' => 2],
            ['title' => 'Post 5', 'body' => 'This is the post 5 body', 'user_id' => 1],
            ['title' => 'Post 6', 'body' => 'This is the post 6 body', 'user_id' => 2],
            ['title' => 'Post 7', 'body' => 'This is the post 7 body', 'user_id' => 1],
            ['title' => 'Post 8', 'body' => 'This is the post 8 body', 'user_id' => 2],
            ['title' => 'Post 9', 'body' => 'This is the post 9 body', 'user_id' => 1]
        ]);
    }
}


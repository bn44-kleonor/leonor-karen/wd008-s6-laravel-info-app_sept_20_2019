<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
        	[
        		'username' => 'admin',
        		'password' => Hash::make('admin123'),
        		'email' => 'admin@gmail.com',
        		'role' => 'admin'
        	],
        	[
        		'username' => 'emma',
        		'password' => Hash::make('emma123'),
        		'email' => 'emma@gmail.com',
        		'role' => 'user'
        	]
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Comment::insert([
        	['post_id' => 1, 'body' => 'This is the comment 1 body', 'user_id' => 1],
        	['post_id' => 2, 'body' => 'This is the comment 2 body', 'user_id' => 2],
        	['post_id' => 3, 'body' => 'This is the comment 3 body', 'user_id' => 1],
            ['post_id' => 4, 'body' => 'This is the comment 4 body', 'user_id' => 2],
            ['post_id' => 5, 'body' => 'This is the comment 5 body', 'user_id' => 1],
            ['post_id' => 6, 'body' => 'This is the comment 6 body', 'user_id' => 2],
            ['post_id' => 7, 'body' => 'This is the comment 7 body', 'user_id' => 1],
            ['post_id' => 8, 'body' => 'This is the comment 8 body', 'user_id' => 2],
            ['post_id' => 9, 'body' => 'This is the comment 9 body', 'user_id' => 1],
            ['post_id' => 1, 'body' => 'This is the comment 1 body', 'user_id' => 2],
            ['post_id' => 2, 'body' => 'This is the comment 2 body', 'user_id' => 1],
            ['post_id' => 3, 'body' => 'This is the comment 3 body', 'user_id' => 2],
            ['post_id' => 4, 'body' => 'This is the comment 4 body', 'user_id' => 1],
            ['post_id' => 5, 'body' => 'This is the comment 5 body', 'user_id' => 2],
            ['post_id' => 6, 'body' => 'This is the comment 6 body', 'user_id' => 1],
            ['post_id' => 7, 'body' => 'This is the comment 7 body', 'user_id' => 2],
            ['post_id' => 8, 'body' => 'This is the comment 8 body', 'user_id' => 1],
            ['post_id' => 9, 'body' => 'This is the comment 9 body', 'user_id' => 2]
        ]);
    }
}

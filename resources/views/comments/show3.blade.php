@extends('layouts.app')


@section('content')

<div class="container">
<div class="row">
<div class="col-2">
<img src="">
</div>
<div class="col-8">
<p>{{$comment->body}}</p>
</div>
<div class="col-2">
<a class="btn btn-primary" href="{{URL::previous()}}">
  Back
  </a>
   <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
     Delete
  </button>
</div>
</div>
</div>

<!-- MODAL -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <h5 class="modal-title" id="deleteModalLabel">Modal title</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
       Do you want to delete comment?
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <form method="POST" action="/comments/{{$comment->id}}">
         <!--  -->
         {{ csrf_field() }}
         {{method_field('DELETE')}}
         <!-- //generates a hidden input field that contains the spoof value -->
         <button type="submit" class="btn btn-danger">Delete Comment</button>
       </form>
     </div>
   </div>
 </div>
</div>
@endsection
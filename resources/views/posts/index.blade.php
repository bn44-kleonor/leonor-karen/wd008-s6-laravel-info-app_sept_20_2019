@extends('layouts.app')
 
@section('content')
<div class="container">
    <h1>Posts</h1>
    {{-- dd($comments) --}}
 
    @foreach($posts as $post)
    {{-- $posts->id --}}
    <div class="card">
        <!-- Posts -->
        <div class="card-body">
            <h5 class="card-title">
                {{ $post->title }}
            </h5>
            <p class="card-text">
                {!! $post->body !!}
            </p>
            <a class="btn btn-primary" href="/posts/{{ $post->id }}">
                View Post
            </a>
        </div>
        <div class="card-footer">
            <!-- Comments -->
            @foreach($comments as $comment)
                @if($comment->post_id == $post->id)
                    <div class="card">
                        <div class="card-body">
                            <div class="card-text">
                                {{ $comment->body }}
                                <a class="btn btn-primary float-right" href="/comments/{{ $comment->id }}">
                                    View Comment
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
 
        </div>
    </div>
    <!-- New Comment Form -->
        <div class="row mt-2">
            <div class="col">
                <form  method="POST" action="/comments" class="bg-dark p-5">
                    {{ csrf_field() }}
                    <div class="form-group my-3" style="width: 95%; margin: 0 auto">
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}> 
                        <textarea class="form-control" id="comment" name="comment" rows="5" placeholder="Write your comment..."></textarea>
                        <button class="btn btn-primary my-2 float-right">Add Comment</button>
                    </div>
                </form>
            </div>
        </div>
    @endforeach
    <div class="col">
        @if (Auth::user()->role == 'admin')
        <div class="ml-auto my-3 float-left">
            <a href="/posts/create" class="btn btn-success">Write a Post</a>
        </div>
        <div class="ml-auto my-3 float-right">
            <!-- {{ $posts->links() }} -->
        </div>
        @endif
    </div>
</div>
@endsection



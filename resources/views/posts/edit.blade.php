@extends('layouts.app')

@section('content')
    <h1>Edit a Post</h1>
    <form method="POST" action="/posts/{{$post->id}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" name="user_id" value="{{ $post->user_id }}"> 
        <div class="form-group">
            <input type="text" name="title" placeholder="Title" class="form-control" value="{{$post->title}}">
        </div>
        <div class="form-group">
            <textarea name="body" placeholder="Body" class="form-control">{{$post->body}}</textarea>            
        </div>
        <button class="btn btn-primary"> Save Changes </button>
    </form>
    <script>
         CKEDITOR.replace( 'body' );
    </script>
@endsection
@extends('layouts.app')

@section('content')
    <h1>Create a Post</h1>
    <form method="POST" action="/posts">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}"> 
        <div class="form-group">
            <input type="text" name="title" placeholder="Title" class="form-control">
        </div>
        <div class="form-group">
            <textarea name="body" placeholder="Body" class="form-control">
                
            </textarea>            
        </div>
        <button class="btn btn-primary"> Create Post </button>
    </form>
    <script>
         CKEDITOR.replace( 'body' );
    </script>
@endsection
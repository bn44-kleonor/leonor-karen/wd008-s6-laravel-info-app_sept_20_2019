<!-- this blade file displays individual comment -->
@extends('layouts.app')

@section('content')
	<!-- {{-- $post --}} -->
	<!-- <h1>Title</h1> -->
	<!-- <h1>{{ $post->title }}</h1> -->
	<!-- <p>Body</p> -->
	<p>{{ $post->body }}</p>
	<a class="btn btn-primary" href="{{ URL::previous() }}">
		Back
	</a>

  @if(Auth::user()->role == 'admin')
  	<button class="btn btn-danger" href="/" data-toggle="modal" data-target="#deleteModal">
  		Delete
  	</button>
    <button class="btn btn-success" href="/" data-toggle="modal" data-target="#deleteModal">
      Create
    </button>
    <button class="btn btn-warning" href="/" data-toggle="modal" data-target="#deleteModal">
      Edit
    </button>
  @endif
@endsection


<!-- DELETE button MODAL -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
       Do you want to delete this post?
     </div>
     <div class="modal-footer">
       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
       <form method="POST" action="/posts/{{ $post->id }}">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button class="btn btn-danger">Delete Post
        </button>
       </form>
      </div>
    </div>
  </div>
</div>
